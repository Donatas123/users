export const types = {
	users: 'users',
	activeUser: 'activeUser',
};

export default () => {
	return {
		[types.users]: [],
		[types.activeUser]: null,
	};
};
