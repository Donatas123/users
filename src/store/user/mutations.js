import {types as userModuleStateTypes} from '@/store/user/state';

export const types = {
	setUsers: 'setUsers',
	setActiveUser: 'setActiveUser',
};

export default {
	[types.setUsers] (store, users) {
		store[userModuleStateTypes.users] = users;
	},

	[types.setActiveUser] (store, activeUser) {
		store[userModuleStateTypes.activeUser] = activeUser;
	},
};
