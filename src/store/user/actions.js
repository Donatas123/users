import axios from 'axios';
import {types as userModuleMutationTypes} from '@/store/user/mutations';

export const types = {
	fetchUsers: 'fetchUsers',
	fetchUser: 'fetchUser',
};

export default {
	async [types.fetchUsers] ({commit}) {
		try {
			const {data} = await axios.get('https://jsonplaceholder.typicode.com/users');

			commit(userModuleMutationTypes.setUsers, data);
		} catch (error) {
			console.error('Something went wrong trying to fetch users');
		}
	},

	async [types.fetchUser] ({commit}, targetUserID) {
		try {
			const {data} = await axios.get(`https://jsonplaceholder.typicode.com/users/${targetUserID}`);

			commit(userModuleMutationTypes.setActiveUser, data);
		} catch (error) {
			console.error('Something went wrong trying to fetch users');
		}
	},
};
