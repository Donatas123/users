import User from '@/pages/User';
import Users from '@/pages/Users';
import VueRouter from 'vue-router';
import Vue from 'vue';

Vue.use(VueRouter);

export default new VueRouter({
	mode: 'history',

	routes: [
		{
			path: '/',
			component: Users,
		},

		{
			path: '/users',
			component: Users,
		},

		{
			path: '/user/:userId',
			component: User,
		},
	],
});
